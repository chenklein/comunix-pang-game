﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Level", menuName = "ScriptableObjects/LevelSettings", order = 1)]
public class LevelSettings : ScriptableObject
{
  [Header("Level ID")]
  public int levelNumber;

  [Header("Diffuclty")]
  public float ballSpeed;
  public int ballDivision;
  public int time;

  [Header("Graphics")]
  public Color ballColor;
  public Image backgroungImage;

  [Header("Rewards")]
  public List<GameObject> rewards;

  [Header("Obsticles")]
  public List<GameObject> obsticles;
}
