﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum GameState
{
  Home,
  InGame,
  Pause,
  Success,
  Fail
}

public class PangModel : PangElement
{

  public GameState gameState;

  public List<LevelSettings> _levels;
  public LevelSettings _currentLevel;

  public int hitPoints;

  public int level;
  public int time;
  public int score;

  public float ballSpeed;
  public int ballDivision;


  public void SetCurrentLevel()
  {
    level = PlayerPrefs.GetInt("CurrentLevel");
    _currentLevel = _levels[level];
    SetLevelSettings();
  }

  public void SetState(GameState _gamestate)
  {
    gameState = _gamestate;
    PangApplication.view.SetGameView(_gamestate);
  }

  public void SetLevelSettings()
  {
    ballSpeed = _currentLevel.ballSpeed;
    ballDivision = _currentLevel.ballDivision;
    time = _currentLevel.time;
    PangApplication.view.SetLevelGraphics(_currentLevel.ballColor, _currentLevel.backgroungImage, _currentLevel.levelNumber, 0, time);

  }
  public void UpdateScore(int scoreToAdd)
  {
    score += scoreToAdd;
    PangApplication.view.SetGameScore(score);
  }

  public void SetLevel(int number)
  {
    level = number;
    PlayerPrefs.SetInt("CurrentLevel", number);
  }

}
