﻿using UnityEngine;
using UnityEngine.UI;

public class PangView : PangElement
{

  [SerializeField] private Transform _topBorder;
  [SerializeField] private Transform _leftBorder;
  [SerializeField] private Transform _rightBorder;
  [SerializeField] private Transform _buttonBorder;

  [SerializeField] private GameObject _homePanel;
  [SerializeField] private GameObject _successPanel;
  [SerializeField] private GameObject _failPanel;
  [SerializeField] private GameObject _gamePanel;
  [SerializeField] private GameObject _pausePanel;

  [SerializeField] private Text _score;
  [SerializeField] private Text _level;

  public Text _time;

  public GameObject player;
  public GameObject ballPrefab;
  public Transform spawnPosition;

  public float screenLimitMin;
  public float screenLimitMax;
  public int currentBallsInGame;

  public LayerMask cameraRay;


  private void Awake()
  {
    _leftBorder.position = Camera.main.ViewportToWorldPoint(new Vector3(0f, 0.5f, 10));
    _rightBorder.position = Camera.main.ViewportToWorldPoint(new Vector3(1, 0.5f, 10));
    _topBorder.position = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 1f, 10));
    _buttonBorder.position = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0f, 10));
    spawnPosition.position = Camera.main.ViewportToWorldPoint(new Vector3(0.15f, 0.75f, 10));

    _homePanel.SetActive(true);
    _gamePanel.SetActive(false);
  }

  public void SetGameView(GameState _gamestate)
  {
    switch (_gamestate)
    {
      case GameState.Home:
        _homePanel.SetActive(true);
        _gamePanel.SetActive(false);
        _failPanel.SetActive(false);
        _successPanel.SetActive(false);
        _pausePanel.SetActive(false);
        break;

      case GameState.InGame:
        _gamePanel.SetActive(true);
        _homePanel.SetActive(false);
        _failPanel.SetActive(false);
        _successPanel.SetActive(false);
        _pausePanel.SetActive(false);
        player.transform.position = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.22f, 10));
        break;

      case GameState.Fail:

        _failPanel.SetActive(true);
        break;

      case GameState.Success:

        _successPanel.SetActive(true);
        break;

      case GameState.Pause:

        _pausePanel.SetActive(true);
        break;
    }
  }
  public void SetGameScore(int score)
  {
    _score.text = "Score : " + score.ToString();
  }

  public void SetLevelGraphics(Color ballcolor, Image backgroundImage, int levelNumber, int score, int time)
  {
    _level.text = "Level : " + levelNumber.ToString();
    _score.text = "Score : " + score.ToString();
    _time.text = "Time : " + time.ToString();

    ballPrefab.GetComponent<SpriteRenderer>().color = ballcolor;
  }
}
