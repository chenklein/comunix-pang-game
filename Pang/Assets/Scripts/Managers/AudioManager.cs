﻿using UnityEngine;

public class AudioManager : PangElement
{
  [SerializeField] AudioSource audioSource;
  public void PlayAudio(AudioClip toPlay)
  {
    audioSource.PlayOneShot(toPlay);
  }
}
