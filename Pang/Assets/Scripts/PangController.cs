﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class PangController : PangElement
{
  private bool _gameIsPaused = false;
  private Timer _timer;
  public FallingItems fallingItems;

  private void Awake()
  {
    _timer = GetComponent<Timer>();
    fallingItems = GetComponent<FallingItems>();
  }
  public void ResetGame()
  {
    foreach (Ball item in Object.FindObjectsOfType<Ball>())
    {
      Destroy(item.gameObject);
    }

    PangApplication.view.player.SetActive(false);
    PangApplication.view.currentBallsInGame = 0;
    PangApplication.model.score = 0;
    PangApplication.model.UpdateScore(0);

    _timer.timerIsRunning = false;

    fallingItems.ResetFallingItemsList();
  }

  public void StartLevel()
  {

    ResetGame();

    // Set Level settings
    PangApplication.model.SetCurrentLevel();

    // Set game state and view
    PangApplication.model.SetState(GameState.InGame);

    // Instantiate initial ball
    GameObject initialBall = Instantiate(PangApplication.view.ballPrefab, PangApplication.view.spawnPosition.localPosition, Quaternion.identity);
    initialBall.GetComponent<Rigidbody2D>().velocity = new Vector2(0.4f, 0.2f);

    // count balls in game to calculate win condition
    PangApplication.view.currentBallsInGame += 1;

    PangApplication.view.player.SetActive(true);

    _timer.timeRemaining = PangApplication.model.time;
    _timer.timerIsRunning = true;

    fallingItems.CretaeFallingItemsList();
  }

  public void LevelWon()
  {
    // Set game state and view
    PangApplication.model.SetState(GameState.Success);

    //Reset level number if total levels are completed, otherwise, increase level number
    if (PangApplication.model.level + 1 < PangApplication.model._levels.Count)
    {
      PangApplication.model.SetLevel(PangApplication.model.level += 1);
    }
    else
    {
      PangApplication.model.SetLevel(0);
    }
  }

  public void LevelFailed()
  {
    // Set game state and view
    PangApplication.model.SetState(GameState.Fail);
  }
  public void QuitGame()
  {
    // Set game state and view
    PangApplication.model.SetState(GameState.Home);
  }

  public void PauseGame()
  {
    if (!_gameIsPaused)
    {
      Time.timeScale = 0f;
      _gameIsPaused = true;
      PangApplication.model.SetState(GameState.Pause);
    }
    else
    {
      Time.timeScale = 1;
      _gameIsPaused = false;
      PangApplication.model.SetState(GameState.InGame);
    }
  }





}

