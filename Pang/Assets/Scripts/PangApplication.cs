﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PangElement : MonoBehaviour
{
  // Gives access to the application and all instances.
  public PangApplication app { get { return GameObject.FindObjectOfType<PangApplication>(); } }
}

public class PangApplication : MonoBehaviour
{
  // Reference to the root instances of the MVC.
  public static PangModel model;
  public static PangView view;
  public static PangController controller;

  [SerializeField] PangModel _model;
  [SerializeField] PangView _view;
  [SerializeField] PangController _controller;

  private void Awake()
  {
    model = _model;
    view = _view;
    controller = _controller;
  }
}

