﻿using UnityEngine;

public class Bullet : PangElement
{
  private const float _speed = 2f;
  private Vector3 _minScale;

  private void OnEnable()
  {
    Invoke("Hide", 1);
    _minScale = PangApplication.view.ballPrefab.transform.localScale / PangApplication.model.ballDivision;
  }

  void Update()
  {
    transform.Translate(Vector2.up * Time.deltaTime * _speed);
  }

  void Hide()
  {
    gameObject.SetActive(false);
  }


  // When bullet hits a ball
  private void OnTriggerEnter2D(Collider2D ball)
  {
    if (ball.gameObject.tag == "Ball")
    {
      PangApplication.view.currentBallsInGame += 1;
      PangApplication.model.UpdateScore(PangApplication.model.hitPoints);

      for (int i = -1; i < 2; i++)
      {
        if (i != 0)
        {
          Vector3 newBallScale = ball.gameObject.transform.localScale / 2;
          if (newBallScale.x != _minScale.x)
          {
            GameObject ballClone = Instantiate(PangApplication.view.ballPrefab, ball.gameObject.transform.localPosition, Quaternion.identity);
            ballClone.transform.localScale = newBallScale;
            ballClone.GetComponent<Rigidbody2D>().velocity = new Vector2(i * 0.7f, PangApplication.model.ballSpeed / 2);
            Destroy(ball.gameObject);
          }
          else
          {
            Destroy(ball.gameObject);
            PangApplication.view.currentBallsInGame -= 1;
          }

          gameObject.SetActive(false);
        }
      }

      DropItem(ball.transform);
    }

    if (PangApplication.view.currentBallsInGame == 0 && PangApplication.model.gameState == GameState.InGame)
    {
      PangApplication.controller.LevelWon();
      PangApplication.view.player.GetComponent<PlayerAnimator>().Rewarded();
    }
  }

  void DropItem(Transform position)
  {
    int randomNumber = Random.Range(0, 2);

    if (randomNumber == 1)
    {
      PangApplication.controller.fallingItems.DropItem(position);
    }
  }

}
