﻿
using UnityEngine;

public class Obsticle : PangElement
{
  private const float _speed = 0.7f;

  private void OnTriggerEnter2D(Collider2D other)
  {
    if (other.transform.tag == "PlayerPaddle" && PangApplication.model.gameState == GameState.InGame)
    {
      PangApplication.controller.LevelFailed();
      PangApplication.view.player.GetComponent<PlayerAnimator>().GetHit();
      GetComponent<Collider2D>().enabled = false;
      Debug.Log("hit obsticle");
    }

  }
  void Update()
  {
    transform.Translate(Vector2.down * Time.deltaTime * _speed);

    if (transform.position.y < PangApplication.view.player.transform.position.y)
    {
      Destroy(gameObject);
    }
  }

}
