﻿
using UnityEngine;

public class Reward : PangElement
{
  private const float _speed = 0.8f;
  private int _rewardScore = 250;

  private void OnTriggerEnter2D(Collider2D other)
  {
    if (other.transform.tag == "PlayerPaddle" && PangApplication.model.gameState == GameState.InGame)
    {
      PangApplication.model.UpdateScore(_rewardScore);
      PangApplication.view.player.GetComponent<PlayerAnimator>().Rewarded();
      GetComponent<Collider2D>().enabled = false;
      Debug.Log("hit reward");
    }
  }

  void Update()
  {
    transform.Translate(Vector2.down * Time.deltaTime * _speed);

    if (transform.position.y < PangApplication.view.player.transform.position.y)
    {
      Destroy(gameObject);
    }
  }
}
