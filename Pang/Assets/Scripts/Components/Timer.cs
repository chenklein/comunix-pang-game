﻿using UnityEngine;

public class Timer : PangElement
{
  public float timeRemaining;
  public bool timerIsRunning = false;

  void Update()
  {
    if (timerIsRunning)
    {
      if (timeRemaining > 0)
      {
        timeRemaining -= Time.deltaTime;
        int seconds = (int)timeRemaining;
        PangApplication.view._time.text = "Time  : " + seconds.ToString();
      }
      else
      {
        timeRemaining = 0;
        timerIsRunning = false;
        PangApplication.controller.LevelFailed();
      }
    }
  }
}
