﻿
using UnityEngine;

public class Ball : PangElement
{
  private Rigidbody2D _rigidBody;

  private void Awake()
  {
    _rigidBody = GetComponent<Rigidbody2D>();
  }
  void OnCollisionEnter2D(Collision2D other)
  {

    if (other.transform.tag == "ButtonBorder")
    {
      float xDif = Random.Range(-1f, 1f);
      _rigidBody.velocity = new Vector2(xDif, PangApplication.model.ballSpeed);// PangModel.instance.ballSpeed);
    }
    else if (other.transform.tag == "LeftBorder")
    {
      float xDif = Random.Range(0f, 1f);
      _rigidBody.velocity = new Vector2(xDif, _rigidBody.velocity.y);

    }
    else if (other.transform.tag == "RightBorder")
    {
      float xDif = Random.Range(-1f, 0f);
      _rigidBody.velocity = new Vector2(xDif, _rigidBody.velocity.y);

    }

    // Loose Condition
    else if (other.transform.tag == "PlayerPaddle" && PangApplication.model.gameState == GameState.InGame)
    {
      PangApplication.view.player.GetComponent<PlayerAnimator>().Fall();
      PangApplication.controller.LevelFailed();
    }

  }


}
