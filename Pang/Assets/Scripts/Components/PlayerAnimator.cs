﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimator : PangElement
{
  private Animator _mAnimator;

  void Awake()
  {
    _mAnimator = GetComponent<Animator>();
  }


  public void WalkLeft()
  {
    transform.rotation = Quaternion.AngleAxis(180, Vector3.up);
    _mAnimator.SetBool("isWalking", true);
  }

  public void WalkRight()
  {
    transform.rotation = Quaternion.AngleAxis(0, Vector3.up);
    _mAnimator.SetBool("isWalking", true);
  }

  public void Idle()
  {
    _mAnimator.SetBool("isWalking", false);
  }

  public void GetHit()
  {
    _mAnimator.SetTrigger("isHit");
  }

  public void Fall()
  {
    _mAnimator.SetTrigger("isFall");
  }

  public void Rewarded()
  {
    _mAnimator.SetTrigger("isRewarded");
  }
}
