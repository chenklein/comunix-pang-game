﻿
using UnityEngine;

public class Player : PangElement
{

  private const float _movementSpeed = 2.0f;
  private bool _touchingPaddle = false;
  private Vector3 _offset;
  private Vector3 _lastPos;
  private Vector2 _currentVelocity;

  private delegate void InputDelegate();
  private InputDelegate _processInput;
  private bool _leftArrowDown;
  private bool _rightArrowDown;

  private PlayerAnimator _playerAnimator;

  private void OnEnable()
  {

#if UNITY_EDITOR
    _processInput = ProcessMouseInput;
#endif

#if UNITY_ANDROID && !UNITY_EDITOR
        _processInput = ProcessTouchInput;
#endif

    _leftArrowDown = _rightArrowDown = false;
  }
  void Awake()
  {
    _playerAnimator = GetComponent<PlayerAnimator>();
  }

  private void Start()
  {
    // Save the last position as the position of the transform when level starts.
    _lastPos = transform.position;
  }

  void Update()
  {
    // If our saved last position is not equal to our current position in this frame.
    if (_lastPos != transform.position)
    {
      // Record the current velocity as the difference.
      _currentVelocity = transform.position - _lastPos;
      _currentVelocity /= Time.deltaTime;
      _lastPos = transform.position;
    }

    if (Input.GetKeyDown(KeyCode.LeftArrow))
    {
      LeftArrowDown();
    }

    if (Input.GetKeyUp(KeyCode.LeftArrow))
    {
      LeftArrowUp();
    }

    if (Input.GetKeyDown(KeyCode.RightArrow))
    {
      RightArrowDown();
    }

    if (Input.GetKeyUp(KeyCode.RightArrow))
    {
      RightArrowUp();
    }

    if (Input.GetKeyDown(KeyCode.Space))
    {
      FireBullets();
    }

    // If the left arrow is being pressed, move the paddle to left 
    if (_leftArrowDown)
    {
      if (transform.position.x > Camera.main.ViewportToWorldPoint(new Vector2(PangApplication.view.screenLimitMin, 0)).x)
        transform.position -= new Vector3(_movementSpeed * Time.deltaTime, 0, 0);
    }

    // If the right arrow is being pressed, move the paddle to right
    if (_rightArrowDown)
    {
      if (transform.position.x < Camera.main.ViewportToWorldPoint(new Vector2(PangApplication.view.screenLimitMax, 0)).x)
        transform.position += new Vector3(_movementSpeed * Time.deltaTime, 0, 0);
    }

    _processInput();  // Process input, whether mouse or touch, dependant on the platform, set in Awake method.
  }


  void ProcessTouchInput()
  {
    Vector3 rayPosition; // Declare a variable on the stack memory to store our mouse position converted to world space.

    // Input class of the Unity is able to detect & count the number of touches on the mobile screen.
    // If that touch count is 0, or more than 1 meaning multiple touches, stop moving by setting the flag to false.
    if (Input.touchCount != 1)
    {
      _touchingPaddle = false;
      return;
    }

    // Input class of the Unity is able to store the touches in an array.
    Touch touch = Input.touches[0];

    Vector2 pos = Input.mousePosition;

    if (touch.phase == TouchPhase.Began)
    {
      Ray ray = Camera.main.ScreenPointToRay(pos);
      RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, 10.0f, PangApplication.view.cameraRay);

      // If hit object has a collider on it.
      if (hit.collider)
      {
        rayPosition = new Vector2(pos.x, pos.y);    // Define the rayPosition struct as a new Vector2 struct consisted of the mouse/touch position components.
        rayPosition = Camera.main.ScreenToWorldPoint(rayPosition);  // Convert ray position from screen point to world point using Unity Engine's remapping method.

        _offset = transform.position - new Vector3(rayPosition.x, 0, rayPosition.z);
        _touchingPaddle = true;    // Make the touching paddle flag true.
      }


    }

    if (_touchingPaddle && touch.phase == TouchPhase.Moved)
    {
      // Update the ray position to follow where the paddle is being dragged.
      rayPosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
      rayPosition = Camera.main.ScreenToWorldPoint(rayPosition);
      rayPosition.x = Mathf.Clamp(rayPosition.x, Camera.main.ViewportToWorldPoint(new Vector2(PangApplication.view.screenLimitMin, 0)).x,
          Camera.main.ViewportToWorldPoint(new Vector2(PangApplication.view.screenLimitMax, 0)).x);

      transform.position = new Vector3(rayPosition.x, _offset.y - transform.position.y, rayPosition.z) + _offset;
    }

    if (_touchingPaddle && (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled))
    {
      _touchingPaddle = false;
    }
  }

  void ProcessMouseInput()
  {
    Vector3 rayPosition;
    Vector2 pos = Input.mousePosition;

    if (Input.GetMouseButtonDown(0))
    {
      Ray ray = Camera.main.ScreenPointToRay(pos);
      RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, 10.0f, PangApplication.view.cameraRay);

      // If hit object has a collider on it.
      if (hit.collider)
      {

        rayPosition = new Vector2(pos.x, pos.y);
        rayPosition = Camera.main.ScreenToWorldPoint(rayPosition);
        _offset = transform.position - new Vector3(rayPosition.x, 0, rayPosition.z);
        _touchingPaddle = true;
      }

    }


    if (_touchingPaddle && Input.GetMouseButton(0))
    {
      // Update the ray position to follow where the paddle is being dragged.
      rayPosition = new Vector2(pos.x, pos.y);
      rayPosition = Camera.main.ScreenToWorldPoint(rayPosition);
      rayPosition.x = Mathf.Clamp(rayPosition.x, Camera.main.ViewportToWorldPoint(new Vector2(PangApplication.view.screenLimitMin, 0)).x,
          Camera.main.ViewportToWorldPoint(new Vector2(PangApplication.view.screenLimitMax, 0)).x);

      transform.position = new Vector3(rayPosition.x, _offset.y - transform.position.y, rayPosition.z) + _offset;
    }

    // If we lift our button off the paddle, stop moving by setting the flag to false.
    if (_touchingPaddle && Input.GetMouseButtonUp(0))
    {
      _touchingPaddle = false;
    }
  }

  public void LeftArrowDown()
  {
    _leftArrowDown = true;
    _playerAnimator.WalkLeft();
  }

  public void RightArrowDown()
  {
    _rightArrowDown = true;
    _playerAnimator.WalkRight();
  }

  public void LeftArrowUp()
  {
    _leftArrowDown = false;
    _playerAnimator.Idle();
  }
  public void RightArrowUp()
  {
    _rightArrowDown = false;
    _playerAnimator.Idle();
  }

  public void FireBullets()
  {
    GameObject bullet = GetComponent<Shooter>().RequestBullet();
    bullet.transform.position = gameObject.transform.localPosition;
  }

}
