﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : PangElement
{
  [SerializeField] private GameObject _bulletPrefb;
  [SerializeField] private GameObject _bulletsContainer;
  [SerializeField] private List<GameObject> _bulletsPool;
  [SerializeField] private int _bullets;

  private void Start()
  {
    _bulletsPool = GenerateBullets(_bullets);
  }

  private List<GameObject> GenerateBullets(int numberOfBullets)
  {
    for (int i = 0; i < numberOfBullets; i++)
    {
      GameObject bullet = Instantiate(_bulletPrefb);
      bullet.SetActive(false);
      _bulletsPool.Add(bullet);

    }

    return _bulletsPool;
  }

  public GameObject RequestBullet()
  {
    foreach (var bullet in _bulletsPool)
    {
      if (bullet.activeInHierarchy == false)
      {
        bullet.SetActive(true);
        return bullet;
      }
    }

    GameObject newBullet = Instantiate(_bulletPrefb);
    _bulletsPool.Add(newBullet);

    return newBullet;

  }
}
