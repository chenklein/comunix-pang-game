﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingItems : PangElement
{

  [SerializeField] private List<GameObject> _items;

  public void CretaeFallingItemsList()
  {
    for (int i = 0; i < PangApplication.model._currentLevel.obsticles.Count; i++)
    {
      _items.Add(PangApplication.model._currentLevel.obsticles[i]);
    }

    for (int i = 0; i < PangApplication.model._currentLevel.rewards.Count; i++)
    {
      _items.Add(PangApplication.model._currentLevel.rewards[i]);
    }
  }

  public void ResetFallingItemsList()
  {
    _items.Clear();
  }

  public void DropItem(Transform instantiatePos)
  {
    if (_items.Count > 0)
    {
      int shuffledItem = Random.Range(0, _items.Count);
      Instantiate(_items[shuffledItem], instantiatePos.position, Quaternion.identity);
      _items.Remove(_items[shuffledItem]);
    }
  }

}
