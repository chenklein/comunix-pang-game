A disclaimer:  I never played a pang game before! I’m relying only on the YouTube video you guys sent.

I'm not a designer, so i used a design package i found on the asset store.

On mobile, the arrow buttons are for moving left and right, and the stone button is for shooting, if used on a pc, left and right arrows for moving and space key for shooting.

Extra credit and why I choose them:

1.Architecture designed with MVC paradigm  - This was the most important one for me, I wanted to learn about this design pattern, and try and implement it correctly, hope I did.

2.Three or more distinct consecutive levels, with increasing difficulty – Sure, this gave me the option to deliver a more complex (and fun) solution, I used scriptable objects to define each level’s properties (Under GameData). level 3 is extremely hard on purpose.


3.Custom visuals and shaders – I think it’s important and helped me present my 2D graphic and animation knowledge.


Tomorrow i'm fully booked, but If I had more time, I would also add audio, multiplayer and leaderboard.
